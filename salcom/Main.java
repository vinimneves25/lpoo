import java.util.Scanner;

class Main {
   public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String nome = sc.nextLine();
        double salario = sc.nextDouble(), vendas = sc.nextDouble(), total = salario + (vendas * 15) / 100;

        System.out.printf("TOTAL = R$ %.2f", total);
    }
}
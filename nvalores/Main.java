import java.util.Scanner;

class Main {
   public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt(), numero, menor = 0;

        for (int i = 0;i < n;i++) {
            numero = sc.nextInt();

            if (i == 0 || numero < menor) {
                menor = numero;
            }
        }

        System.out.print("Menor Valor = " + menor);
    }
}
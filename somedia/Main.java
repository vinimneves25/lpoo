import java.util.Scanner;

class Main {
   public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        float n[] = new float[10], soma = 0, media = 0;

	for(int i = 0;i < 10;i++) {
	    n[i] = sc.nextFloat();
	    soma += n[i];
	}

	media = soma / 10;

	System.out.println("Soma = " + (int) soma);
	System.out.printf("Média = %.2f\n", media);
    }
}

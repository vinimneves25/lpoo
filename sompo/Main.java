import java.util.Scanner;

class Main {
   public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] vetor = new int[12];

        for (int i = 0;i < 12;i++) {
            vetor[i] = sc.nextInt();
        }

        int n1 = sc.nextInt(), n2 = sc.nextInt();
        if (n1 < 0 || n1 > 11) {
            System.out.print("Entrada de X inválida.");
        } else if (n2 < 0 || n2 >11) {
            System.out.print("Entrada de Y inválida.");
        } else {
            System.out.print("Soma = " + (vetor[n1] + vetor[n2]));
        }
                
    }
}
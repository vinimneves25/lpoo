import java.util.Scanner;

class Main {
   public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
	    int numeros[] = new int[4], aux;

		for (int i = 0;i < 4;i++) {
			numeros[i] = sc.nextInt();
			for (int x = 0;x < i;x++) {
				if (numeros[i] < numeros[x]) {
					aux = numeros[i];
					numeros[i] = numeros[x];
					numeros[x] = aux;
				}
			}
		}

		sc.close();

		for (int i = 0;i < 4;i++) {	
			System.out.print(numeros[i] + " ");
		}
	}
}

import java.util.Scanner;

class Main {
   public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        float[] v1 = new float[2], v2 = new float[2];
        double distancia = 0;

        v1[0] = sc.nextFloat();
        v1[1] = sc.nextFloat();

        v2[0] = sc.nextFloat();
        v2[1] = sc.nextFloat();

        distancia = Math.sqrt(Math.pow(v2[0] - v1[0], 2) + Math.pow(v2[1] - v1[1], 2));

        System.out.printf("%.4f", distancia);
    }
}
import java.util.Scanner;

class Main {
   public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String produtos[][] = {
            {"1", "Cachorro quente", "4.00"},
            {"2", "X-salada", "4.5"},
            {"3", "X-bacon", "5"},
            {"4", "Torrada simples", "2"},
            {"5", "Refrigerante", "1.5"}
        };

        int id = sc.nextInt(), qtd = sc.nextInt();
        double total = 0;
        for (int i = 0;i < 5;i++) {
            if (Integer.parseInt(produtos[i][0]) == id) {
                total = Double.parseDouble(produtos[i][2]) * qtd;
            }
        }

        System.out.printf("Total: R$ %.2f", total);
    }
}


import java.util.Scanner;

class Main {
   public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String[] nomes = new String[5];

        for (int i = 0;i < 5;i++) {
            nomes[i] = sc.nextLine();
        }

        String busca = sc.nextLine();
        Integer posicao = null;
        for (int i = 0;i < 5;i++) {
            if (nomes[i].equals(busca)) { 
                posicao = i;
            }
        }

        if (posicao != null) {
            System.out.println("Valor encontrado na posição " + posicao + ".");
        } else {
            System.out.println("Valor não encontrado.");
        }
        
    }
}
import java.util.Scanner;

class Main {
    public static void main(String[] args) {
	Scanner sc = new Scanner(System.in);

	int idade = sc.nextInt(), anos = idade / 365, meses = (idade - anos * 365) / 30, dias = idade - meses * 30 - anos * 365;

	System.out.println(anos + " ano(s)\n" + meses + " mes(es)\n" + dias + " dia(s)");
    }
}

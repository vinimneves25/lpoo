import java.util.Scanner;

class Main {
   public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[][] m1 = new int[4][4], m2 = new int[4][4];

        for (int i = 0;i < 4;i++) {
            for (int x = 0;x < 4;x++) {
                m1[i][x] = sc.nextInt();
            }
        }

        for (int i = 0;i < 4;i++) {
            for (int x = 0;x < 4;x++) {
                m2[i][x] = sc.nextInt();
            }
        }

        Integer maior = null;
        for (int i = 0;i < 4;i++) {
            for (int x = 0;x < 4;x++) {
                if (m1[i][x] > m2[i][x]) {
                    //System.out.print(m1[i][x] + " ");
                    maior = m1[i][x];
                } else {
                    //System.out.print(m1[i][x] + " ");
                    maior = m2[i][x];
                }

                System.out.print(maior + " ");
            }

            System.out.print("\n");
        }
    }
}
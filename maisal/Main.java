import java.util.Scanner;
import java.util.ArrayList;

class Main {
   public static void main(String[] args) {
        double maiores[] = new double[]{0, 0, 0}, salario;
        ArrayList<Double> salarios = new ArrayList<>();
        Scanner sc = new Scanner(System.in);

        do {
            salario = sc.nextDouble();

            if(salario == 0) {
                continue;
            }

            salarios.add(salario);
        } while (salario != 0);

        for(int i = 0;i < 3;i++) {
            for(int x = 0;x < salarios.size();x++){
                if(salarios.get(x) > maiores[i]){
                    maiores[i] = salarios.get(x);
                    x = 0;
                }
            }

            salarios.remove(salarios.indexOf(maiores[i]));
            System.out.printf("%.2f\n", maiores[i]);
        }
    }
}

